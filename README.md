# Tutorials for the Cracow School of Theoretical Physics/MCnet Summer School in Zakopane (2022)

[WEBSITE](https://indico.cern.ch/event/1104027/)

Schedule still to be determined:
 * [Herwig7](herwig)
 * [Madgraph](madgraph)
 * [Pythia8](pythia)
 * [Sherpa](sherpa)
 * [Rivet](rivet)
 * [Common project](project)


## Prerequisites

Most tutorials will be based on Docker.
If you are using MacOS or Windows, you will first need to create a DockerID at https://hub.docker.com/signup

Head to https://docs.docker.com/install for installation instructions.

You can check that Docker has installed properly by running the `hello-world` Docker image

        $ docker run hello-world

Some helpful commands:

`docker run [OPTIONS] IMAGE` to run an image; 
`docker ps` to list active containers; 
`docker image ls` to list available images/apps; 
`docker attach [OPTIONS] CONTAINER` to attach to a container

When you are running inside a container, you can use `CTLR-p CTLR-q` to detach from it and leave it running. 

Please see tutorial-specific instructions as well.


