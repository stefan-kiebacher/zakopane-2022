# Common project

**NB: It would be advisable to follow the prerequisites (below) ahead of the tutorial to avoid bandwidth issues.**


## Prerequisites


For this tutorial, we will be using Docker. To download the image for a given generator and Rivet, simply type e.g.:

```
  docker pull hepstore/rivet-pythia:3.1.6
```

or a similar command for the `rivet-herwig` and `rivet-sherpa` images.
We have provided a little `setup.sh` script which will let you use the image software outside of the container

You can source the script like so

```
  source setup.sh
```

which should give you access to all relevant command-line tools. To check that it worked, try e.g.

```
  rivet --version
```

which should return `rivet v3.1.6`.

Alternatively, you can run the container interactively, e.g. like so

```
  docker container run -it hepstore/rivet-pythia:3.1.6 /bin/bash
```
If you would like to have access to your current directory also from within the container, 
you can also use the following variation of the above command:
```
  docker container run -it -v $PWD:$PWD -w $PWD hepstore/rivet-pythia:3.1.6 /bin/bash
```

Have fun!

