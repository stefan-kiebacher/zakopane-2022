BEGIN PLOT /MY_W_ANALYSIS/lep_pT1
XLabel=leading lepton $p_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}$ [pb GeV$^{-1}$]
LogX=1
END PLOT

BEGIN PLOT /MY_W_ANALYSIS/met
XLabel=$p_\mathrm{T}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}^\mathrm{miss}$ [pb GeV$^{-1}$]
LogX=1
END PLOT

